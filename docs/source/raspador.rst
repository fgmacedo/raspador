
raspador
========

O módulo raspador fornece estrutura genérica para extração de dados a partir de
arquivos texto semi-estruturados.


Analizador
----------

.. automodule:: raspador.analizador
    :members:


Campos
------

.. automodule:: raspador.campos
    :members:
    :undoc-members:


Coleções
--------

.. automodule:: raspador.colecoes
    :members:
    :undoc-members:

